<?php

namespace wangbj\upload\engine;

use wangbj\upload\UploadValidate;
use think\Request;
use think\Exception;

/**
 * 存储引擎抽象类
 */
abstract class Server
{
    protected $file;
    protected $error;
    protected $fileName;
    protected $fileInfo;

    // 是否为内部上传
    protected $isInternal = false;
    // 上传文件格式验证
    protected $uploadValidate = UploadValidate::class;

    /**
     * 构造函数
     */
    protected function __construct()
    {
    }

    /**
     * 设置文件格式验证
     * @param $validate
     * @return void
     */
    public function setUploadValidate($validate=null){
        if(empty($validate)) $validate = UploadValidate::class;
        $this->uploadValidate = $validate;
    }

    /**
     * 获取文件格式验证
     * @return mixed|string
     */
    public function getUploadValidate(){
        return $this->uploadValidate;
    }

    /**
     * 设置上传的文件信息
     * @param $name
     * @return void
     * @throws Exception
     */
    public function setUploadFile($name)
    {
        // 接收上传的文件
        $this->file = request()->file($name);
        if (empty($this->file)) {
            throw new Exception('未找到上传文件的信息');
        }
        // 校验文件
        $uploadValidate = $this->getUploadValidate();
        if(!empty($uploadValidate)) {
            $result = validate(UploadValidate::class)->check(['file' => request()->file($name)]);
            if (true !== $result) {
                throw new Exception($result);
            }
        }
        // 文件信息
        $this->fileInfo = [
            'size'     => $this->file->getSize(),
            'mime'     => $this->file->getMime(),
            'name'     => $this->file->getInfo('name'),
            'realPath' => $this->file->getRealPath(),
        ];
        // 生成保存文件名
        $this->fileName = $this->buildSaveName();
    }

    /**
     * 设置上传的文件信息
     * @param $filePath
     * @return void
     */
    public function setUploadFileByReal($filePath)
    {
        // 设置为系统内部上传
        $this->isInternal = true;
        // 文件信息
        $this->fileInfo = [
            'name' => basename($filePath),
            'size' => filesize($filePath),
            'tmp_name' => $filePath,
            'error' => 0,
        ];
        // 生成保存文件名
        $this->fileName = $this->buildSaveName();
    }

    /**
     * 抓取网络资源
     * @param $url
     * @param $key
     * @return mixed
     */
    abstract protected function fetch($url, $key);

    /**
     * 文件上传
     * @param $save_dir
     * @return mixed
     */
    abstract protected function upload($save_dir);

    /**
     * 文件删除
     * @param $fileName
     * @return mixed
     */
    abstract protected function delete($fileName);

    /**
     * 返回上传后文件路径
     * @return mixed
     */
    abstract public function getFileName();

    /**
     * 返回文件信息
     * @return mixed
     */
    public function getFileInfo()
    {
        return $this->fileInfo;
    }

    /**
     * 获取真实路径
     * @return mixed
     */
    protected function getRealPath()
    {
        return $this->fileInfo['realPath'];
    }

    /**
     * 返回错误信息
     * @return mixed
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * 生成保存文件名
     */
    private function buildSaveName()
    {
        // 要上传图片的本地路径
        $realPath = $this->getRealPath();
        // 扩展名
        $ext = pathinfo($this->getFileInfo()['name'], PATHINFO_EXTENSION);
        // 自动生成文件名
        return date('YmdHis') . substr(md5($realPath), 0, 5)
            . str_pad(rand(0, 9999), 4, '0', STR_PAD_LEFT) . ".{$ext}";
    }

}

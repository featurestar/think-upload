<?php
namespace wangbj\upload\facade;

use think\Facade;

/**
 * Class Captcha
 * @package think\captcha\facade
 * @mixin \wangbj\afs\AfsManage
 */
class Upload extends Facade
{
    protected static function getFacadeClass()
    {
        return \wangbj\upload\UploadManage::class;
    }
}
<?php
// +----------------------------------------------------------------------
// | Upload 配置文件
// +----------------------------------------------------------------------
use wangbj\upload\UploadValidate;
return [
    // 默认引擎
    'default' => env('UPLOAD_DEFAULT', 'local'),
    // 上传文件格式校验
    'validate' => env('UPLOAD_VALIDATE', UploadValidate::class),
    // 七牛云
    'qiniu' => [
        "bucket" => env('UPLOAD_QINIU_BUCKET', ""),
        "access_key" => env('UPLOAD_QINIU_ACCESS_KEY', ""),
        "secret_key" => env('UPLOAD_QINIU_SECRET_KEY', ""),
        "domain" => env('UPLOAD_QINIU_DOMAIN', ""),
    ],
    // 阿里云
    'aliyun' => [
        "bucket" => env('UPLOAD_ALIYUN_BUCKET', ''),
        "access_key_id" => env('UPLOAD_ALIYUN_ACCESS_KEY_ID', ''),
        "access_key_secret" => env('UPLOAD_ALIYUN_ACCESS_KEY_SECRET', ''),
        "domain" => env('UPLOAD_ALIYUN_DOMAIN', ''),
    ],
    // 腾讯云
    'qcloud' => [
        "bucket" => env('UPLOAD_QCLOUD_BUCKET', ''),
        "region" => env('UPLOAD_QCLOUD_REGION', ''),
        "secret_id" => env('UPLOAD_QCLOUD_SECRET_ID', ''),
        "secret_key" => env('UPLOAD_QCLOUD_SECRET_KEY', ''),
        "domain" => env('UPLOAD_QCLOUD_DOMAIN', ''),
    ],
];
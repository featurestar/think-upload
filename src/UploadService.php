<?php

namespace wangbj\upload;

use think\Service;
use think\facade\Config;
class UploadService extends Service {

    public function register()
    {
        $this->app->bind('upload', UploadManage::class);
    }

    public function boot()
    {
        // 设置config
        app('upload')->setConfig(Config::get('upload'));
        // 注册引擎
        app('upload')->storage();
    }
}
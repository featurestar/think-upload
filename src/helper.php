<?php
use wangbj\upload\UploadManage;
if (!function_exists('upload')) {
    function upload($engine = null): UploadManage
    {
        $upload = new UploadManage();

        $upload->storage($engine);

        return $upload;
    }
}
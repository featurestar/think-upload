<?php
// +----------------------------------------------------------------------
// | likeshop100%开源免费商用商城系统
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | 商业版本务必购买商业授权，以免引起法律纠纷
// | 禁止对系统程序代码以任何目的，任何形式的再发布
// | gitee下载：https://gitee.com/likeshop_gitee
// | github下载：https://github.com/likeshop-github
// | 访问官网：https://www.likeshop.cn
// | 访问社区：https://home.likeshop.cn
// | 访问手册：http://doc.likeshop.cn
// | 微信公众号：likeshop技术社区
// | likeshop团队 版权所有 拥有最终解释权
// +----------------------------------------------------------------------
// | author: likeshopTeam
// +----------------------------------------------------------------------
namespace wangbj\upload;
use think\Validate;

class UploadValidate extends Validate
{

    protected $rule = [
        'file' => 'fileExt:jpg,jpeg,gif,png,bmp,tga,tif,pdf,psd,avi,mp4,mp3,wmv,mpg,mpeg,mov,rm,ram,swf,flv,pem,ico',
    ];

    protected $message = [
        'file.fileExt' => '该文件类型不允许上传',
    ];


    /**
     * 切面验证接收到的参数
     * @param $scene
     * @param $validate_data
     * @return array
     * @throws \Exception
     */
    public function goCheck($scene=null,$validate_data = [])
    {
        // 1.接收参数
        $params = request()->param();
        //合并验证参数
        $params = array_merge($params,$validate_data);

        // 2.验证参数
        if (!($scene ? $this->scene($scene)->check($params) : $this->check($params))) {
            $exception = is_array($this->error)
                ? implode(';', $this->error) : $this->error;
            throw new \Exception($exception);
        }
        // 3.成功返回数据
        return $params;
    }
}
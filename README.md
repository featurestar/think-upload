# think-config

#### 介绍
自定义一个上传服务[整合本地、七牛云、阿里云、腾讯云]
环境: ThinkPHP:8.0 PHP:8.1 理论上 php >= 7.1 thinkPHP >= 5.0 都可以使用
### 安装教程

```shell
# 安装
$ composer require wangbj/think-upload
```

### 使用说明

```php
# 1、生成配置文件 - composer 是自动执行这一步的 如果没生成config文件可以手动执行
$ php think vendor:publish

# 2、修改配置文件 /config/afs.php 或在 /.env 文件中添加配置
#UPLOAD
UPLOAD_DEFAULT=local
# local aliyun qiniu qcloud
UPLOAD_VALIDATE=wangbj\upload\UploadValidate
#UPLOAD_QINIU
UPLOAD_QINIU_BUCKET=
UPLOAD_QINIU_ACCESS_KEY=
UPLOAD_QINIU_SECRET_KEY=
UPLOAD_QINIU_DOMAIN=
#UPLOAD_ALIYUN
UPLOAD_ALIYUN_BUCKET=
UPLOAD_ALIYUN_ACCESS_KEY_ID=-
UPLOAD_ALIYUN_ACCESS_KEY_SECRET=
UPLOAD_ALIYUN_DOMAIN=
#UPLOAD_QCLOUD
UPLOAD_QCLOUD_BUCKET=
UPLOAD_QCLOUD_REGION=
UPLOAD_QCLOUD_SECRET_ID=
UPLOAD_QCLOUD_SECRET_KEY=
UPLOAD_QCLOUD_DOMAIN=
```

### 目录说明

```
├─ src
│   ├─ config           # 配置文件目录
│   │   └─ upload.php   # 配置文件
│   ├─ engine           # 存储引擎文件
│   │   └─ Aliyun.php   # 阿里云OSS引擎
│   │   └─ Local.php    # 本地引擎
│   │   └─ Qcloud.php   # 腾讯云COS引擎
│   │   └─ Qiniu.php    # 七牛云引擎
│   │   └─ Server.php   # 公共引擎-其他引擎都继承它
│   ├─ facades          # 门面文件目录
│   │   └─ Upload.php   # 门面
│   └─ helper.php       # 帮助文件
│   └─ UploadManage.php # 项目主文件
│   └─ UploadService.php    # TP使用的Service文件
│   └─ UploadValidate.php   # TP的文件验证
└─ composer.json        # 配置

```

### 使用方式

```php
# 引入门面
use wangbj\upload\facade\Upload;
# 随便加一个路由
Route::get('/upload', function(){
    
    $upload = Upload::storage(); 
    # 上面这三种方式都能拿到upload类
    
    $save_dir = 'uploads/images';
    $upload = app('upload');
    # 写入文件
    $upload->setUploadFile('file');
    # 上传文件
    if (!$upload->upload($save_dir)) throw new Exception($upload->getError());
    # 获取文件名 -上传后的文件名
    $fileName = $upload->getFileName();
    # 获取文件信息
    $fileInfo = $upload->getFileInfo();
    // 3、处理文件名称
    if (strlen($fileInfo['name']) > 128) {
        $file_name = substr($fileInfo['name'], 0, 123);
        $file_end = substr($fileInfo['name'], strlen($fileInfo['name'])-5, strlen($fileInfo['name']));
        $fileInfo['name'] = $file_name.$file_end;
    }
    // 具体路径
    $uri = $save_dir . '/' . str_replace("\\","/", $fileName);
    $uri = $upload->getFileUrl($uri);
    dd($fileName, $fileInfo, $uri);
    // 输出结果
//    1 "20230808225609184211191.png"
//    2 array:5 [
//        "ext" => "png"
//        "size" => 26696
//        "mime" => "image/png"
//        "name" => "logo-400.png"
//        "realPath" => "/tmp/phpKD8zDz"
//    ]
//    3 "http://tp.sm.com/uploads/images/20230808225609184211191.png"
    
});
```

### 备注
-- 本项目在[stevema/think-upload](https://gitee.com/steve_ma/think-upload)基础上优化而来,在此特别感谢stevema的贡献!
